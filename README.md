# Artificial Intelligence

Berdasarkan dataset yg ada di https://www.kaggle.com/vzrenggamani/hanacaraka  

Cobalah :
- Buatlah program tensorflow
  - meng-capture frame melalui kamera (dengan opencv atau yg lain)
  - mendeteksi minimal 2 huruf dan meng-output-kan hasil deteksi

- Kirimkan :
  - program tensorflow beserta H5 nya
  - langkah pembuatan program
  - rasio dataset yg digunakan untuk training, test, validation
  - jumlah dataset yg digunakan untuk training, test, validation

Catatan :
- Bahasa yg digunakan terbatas antara c, c++, python atau javascript
- Pengerjaan tidak harus urut & tidak harus dikerjakan semua
- Usahakan program di-unggah ke git
- 1 frame hanya berisi satu huruf aksara jawa
- Tambahkan comment di program untuk mempermudah pembacaan source code
